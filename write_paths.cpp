
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for computing network flows.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/format.hpp>
#include "nnt/auxiliary.h"

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the data.
  //============================================================================

  wn_user::libnucnet_data my_libnucnet_data( v_map );

  //============================================================================
  // Get the zones to study.  Sort them first.
  //============================================================================

  Libnucnet__setZoneCompareFunction(
    my_libnucnet_data.getNucnet(),
    (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
  );

  std::vector<nnt::Zone> zones = my_libnucnet_data.getVectorOfZones();

  //============================================================================
  // Graph.
  //============================================================================

  wn_user::network_grapher my_network_grapher( v_map );

  my_network_grapher.write( zones );

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
