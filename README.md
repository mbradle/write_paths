# write_paths #

This project creates chart of the nuclide diagrams that show paths between species.

### Steps to install ###

First, clone the repository by typing 

**git clone https://bitbucket.org/mbradle/write_paths.git**

Next, ensure that wn_user is installed.  If you have not alreadly done so, type

**git clone https://bitbucket.org/mbradle/wn_user.git**

Change into the *write_paths* directory and create the project by typing

**cd write_paths**

**./project_make**

## Steps to create the diagram files ##

To get started, retrieve an example xml file by typing

**curl -o example.xml -J -L  https://osf.io/r8dvw//download**

Create a directory, preferably called *output*, where the output [graphviz](https://graphviz.org) files will be stored:

**mkdir output**

Run the *write_paths* code on a previously created *paths* output xml file.  As an initial example, use the example input file that you retrieved over the web.  Thus, type

**./write_paths --libnucnet_xml example.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 26 and z <= 42 and a - z >= 26 and a - z <= 60]"**

This example execution will read the data *example.xml* file and
create the path diagrams from those data.  It will create a subset of the full network
with atomic number *Z* in the range 26 <= Z <= 42 and neutron number *N = A - Z*, where *A* is the mass number,
in the range 26 <= N <= 60.
The output from the execution will be a number
[graphviz](https://graphviz.org) files (labeled *out_x.dot*, where *x* is the
sequential label) in the directory *output*.  To run on your own output xml file called, say,
*my_out.xml*, first clean up the output directory by typing

**rm output/\***

Then execute the code on your file by typing, for example,

**./write_paths --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 26 and z <= 42 and a - z >= 26 and a - z <= 60]"**

Use other options, as desired.  To see the other options available in running the *write_paths* code, type

**./write_paths --help**

and

**./write_paths --prog all**

For example, since you might only be interested in the first 10 paths.  In this case,
you can use a *zone_xpath*:

**./write_paths --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 26 and z <= 42 and a - z >= 26 and a - z <= 60]" --zone_xpath "[position() <= 10]"**

## Steps to create pdf files ##

Once you have created the appropriate *dot* files, you can convert them into pdfs.  To do so, follow these [steps](https://osf.io/36apc/wiki/home/).

## Steps to animate the diagrams ##

It is possible to combine the pdf files previously created into an animation.
To do so, follow these [steps](https://osf.io/792fy/wiki/home/)

